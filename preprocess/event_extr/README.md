# Preprocess BEL data

Follow the steps below to create training dataset:

1. clean XML files:
	
	cd ./preprocess/extra
	chmod +x ./clean_dataset.sh
	./clean_dataset.sh <dir/with/xml/files>


2. preprocess dicts:

	python -m preprocess.extra.dicts pre --gobp <GOBP/dict/file> --hgnc <HGNC/dict/file> --mgi <MGI/dict/file> --meshd <MESHD/dict/file> --outdir <DICTS/parsed>

3. preprocess dataset:

	python -m preprocess.extra.dicts pre --indir <dir/with/xml/files> --dicts-dir <DICTS/parsed> --outdir <dir/with/parsed/ds>

4. postprocess dicts:

	python -m preprocess.extra.dicts post --dict-dir <DICTS/parsed> <dir/with/parsed/ds> --ds-dir <dir/with/parsed/ds>

	
