#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')
logger = logging.getLogger(__name__)


import argparse
import pandas as pd
from src.biosyn.preprocesser import TextPreprocess
from ieaiaio import IOUtils as iou

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Process dictionaries.')
    
    subparsers = parser.add_subparsers(help='Stages: before <pre> or after <post> dataset creation. Use <stage> -h to see options.', dest='stage')
    
    pre_parser = subparsers.add_parser("pre", help="Preprocess raw dictionaries file")
    

    pre_parser.add_argument('--gobp', default = None, type = str, help="Path to GOBP dictionary")
    pre_parser.add_argument('--hgnc', default = None, type = str, help="Path to HGNC dictionary")
    pre_parser.add_argument('--mgi', default = None, type = str, help="Path to MGI dictionary")
    pre_parser.add_argument('--meshd', default = None, type = str, help="Path to MeSH (Disease) dictionary")
    pre_parser.add_argument('--outdir', required=True, type = str, help="Dir where to store dicts in BioSyn format")
    
    
    post_parser = subparsers.add_parser("post", help="Postprocess dictionaries (include train mentions to increase coverage). Save in datasets folder")
    post_parser.add_argument('--dicts', required=True, type = str, help="Dir where dicts in BioSyn format are stored")
    post_parser.add_argument('--datasets', required=True, type = str, help="Dir where datasets are stored")
    
    return parser.parse_args()

def parse_gobp_dict(infile, outdir):
    
    outfile = iou.join_paths([outdir,"gobp.dict"])
    
    with open(infile) as infile, open(outfile, "w") as out:
        for line in infile:
            symbol, synset = line.strip().split("||")
            symbols = [s.strip() for s in symbol.split("|")]
            
            for symbol in symbols:
                for synonym in synset.split("|"):
                    out.write("{}\n".format("||".join([symbol, synonym.strip()])))


def parse_hgnc_dict(infile, outdir):
    
    synset_column_names = ["Approved name", "Previous symbols", "Alias symbols", "Previous name" , "Alias names"]
    
    outfile = iou.join_paths([outdir,"hgnc.dict"])
    
    df = pd.read_csv(infile, sep = "\t")
    
    with open(outfile, "w") as out:
        for idx,row in df.iterrows():
            rowdict = row.to_dict()
            hgnc_id = rowdict.get("HGNC ID")
            symbol = str(rowdict.get("Approved symbol"))
            synset = [symbol]
            for cname in synset_column_names:
                values = rowdict.get(cname)
                
                if pd.isnull(values):
                    continue
                
                # values can be list
            
                values = str(values).split(",")
      
                synset += values
            
            synset = [s.replace('"','').strip() for s in synset]
            synset = [s for s in synset if not "/data/gene-symbol-report/#!/" in s]
                        
            for synonym in synset:
                out.write("{}||{}\n".format(hgnc_id,synonym))
                
                
def parse_mgi_dict(infile, outdir):
    
    synset_column_names = ["Marker Synonyms (pipe-separated)"]
    
    outfile = iou.join_paths([outdir,"mgi.dict"])    
    
    reader = pd.read_csv(infile, sep = "\t", chunksize = 10000)
    
#    print(next(reader).T.head(n=20))
    
    with open(outfile, "w") as out:
        
        for df in reader:
            
            df = df[df["Marker Type"].isin(["Gene","Pseudogene"])]
            df = df[df["Status"] != "W"]
            
            for idx,row in df.iterrows():
                rowdict = row.to_dict()
                mgi_id = rowdict.get("MGI Accession ID")
                symbol = rowdict.get("Marker Symbol")
                
                if pd.isnull(symbol) or pd.isnull(mgi_id):
                    continue 
                
                synset = [str(symbol)]
                for cname in synset_column_names:
                    values = rowdict.get(cname)
                    
                    if pd.isnull(values):
                        continue
                    
                    # values can be list
                    values = str(values).split("|")
          
                    synset += values
                
                synset = [str(s).strip() for s in synset]
                synset = [s for s in synset if not "withdrawn" in s]
                                
                for synonym in synset:
                    out.write("{}||{}\n".format(mgi_id,synonym))
                    
def parse_meshd_dict(infile, outdir):
    
    tpp = TextPreprocess(lowercase=True, remove_punctuation=True)
    
    synset_column_names = ["Synonyms"]
    
    outfile = iou.join_paths([outdir,"meshd.dict"]) 
        
    df = pd.read_csv(infile, sep = ",")
    
    with open(outfile, "w") as out:
        for idx,row in df.iterrows():
            rowdict = row.to_dict()
            meshd_id = rowdict.get("DiseaseID")
            
            if "OMIM" in meshd_id:
                continue
            
            symbol = rowdict.get("DiseaseName")
            
            synset = [str(symbol)]
            
            for cname in synset_column_names:
                values = rowdict.get(cname)
                 
                if pd.isnull(values):
                    continue
                
                synset += values.split("|")
                
                
            synset = [str(s).strip() for s in synset]
                
            for synonym in synset:
                synonym = tpp.run(synonym)
                out.write("{}||{}\n".format(meshd_id,synonym))




class PostProcessDicts(object):
    
    def __init__(self, logger = None):
        
        self.logger = logger or logging.getLogger(self.__class__.__name__)
        self.tpp = TextPreprocess(lowercase=True, remove_punctuation=True)
        self.name2cui = {}
        
    
    def format_dict_entry(self,name, cui):
        
        entry = "{}\n".format("||".join([cui,name]))
        
        return entry

    def augment_dict_with_mentions_from_split(self, indir, dictname, split):
        
        
        tmp_tot = len(self.name2cui)
        
        for filepath in iou.iglob(indir, ext = "concept"):
            
            with open(filepath) as infile:
                for line in infile:
                    _, _, _, name, cui = line.strip().split("||")
                    
                    if dictname == "MESHD":
                        cui = self.tpp.run(cui)
                    
                    if name not in self.name2cui:
                        self.name2cui[name] = cui
                    # If existing name and new cui with it, concat the cui
                    elif name in self.name2cui and cui not in self.name2cui[name]:
                        self.name2cui[name] += "|"+cui
        
        tot = len(self.name2cui)
        
        self.logger.info("#{} - Added {} (mention,cui) pairs to {} dictionary".format(dictname, tot-tmp_tot, split.upper()))
                
        
    def write_dict(self,outpath):
        
        with open(outpath, "w") as outfile:
            for name,cui in self.name2cui.items():
                entry = self.format_dict_entry(name =name, cui = cui)
                outfile.write(entry)
    
    
    
    def reset_dict(self):
        
        self.name2cui = {}
        
        
    def create_dict_files(self, indir, outdir):
        
        for dictpath in iou.iglob(indir, ext = "dict", recursive = True):
            dictname = iou.fname(dictpath).replace(".dict","").upper()
            ds_outdir = iou.join_paths([outdir, dictname])
            
            with open(dictpath) as infile:
                for line in infile:
                    cui,name = line.strip().split("||")
                    self.name2cui[name] = cui
                    
            for split in ["train","dev"]:
                if split == "train":
                    ds_dictpath = iou.join_paths([ds_outdir, "train_dictionary.txt"])
                    self.write_dict(ds_dictpath)
                    
                elif split in ["dev","test"]:
                    
                    filename = "{}_dictionary.txt".format(split)
                    
                    ds_dictpath = iou.join_paths([ds_outdir, filename])
                    
                    augmentation_split = "train" if split == "dev" else "dev"
                    augmentation_split_dirname = "processed_{}".format(augmentation_split)
                     
                    self.augment_dict_with_mentions_from_split(indir = iou.join_paths([ds_outdir,augmentation_split_dirname]), 
                                                                dictname = dictname,
                                                                split = split)
                     
                    self.write_dict(ds_dictpath)
                    
            self.reset_dict()
                                
if __name__ == "__main__":
    
    args = parse_args()
    

    if args.stage == "pre":    
        if args.gobp is not None:
            outdir = iou.join_paths([args.outdir,"GOBP"])
            if not iou.exists(outdir):
                logger.info("Preprocessing GOBP dictionary")
                iou.mkdir(outdir)
                parse_gobp_dict(infile = args.gobp, outdir = outdir)
            
        if args.hgnc is not None:
            
            outdir = iou.join_paths([args.outdir,"HGNC"])
            if not iou.exists(outdir):
                logger.info("Preprocessing HGNC dictionary")
                iou.mkdir(outdir)
                parse_hgnc_dict(infile = args.hgnc, outdir = outdir)
            
        if args.mgi is not None:
            outdir = iou.join_paths([args.outdir,"MGI"])
            if not iou.exists(outdir):
                logger.info("Preprocessing MGI dictionary")
                iou.mkdir(outdir)
                parse_mgi_dict(infile = args.mgi, outdir = outdir)
            
        if args.meshd is not None:
            outdir = iou.join_paths([args.outdir,"MESHD"])
            if not iou.exists(outdir):
                logger.info("Preprocessing MESHD dictionary")
                iou.mkdir(outdir)
                parse_meshd_dict(infile = args.meshd, outdir = outdir)
    
    elif args.stage == "post":
        
        ppd = PostProcessDicts()
        
        ppd.create_dict_files(indir = args.dicts, outdir = args.datasets)
        
        
        
        
        
    
    


