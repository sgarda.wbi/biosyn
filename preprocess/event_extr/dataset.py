#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

import argparse
import bioc
from collections import defaultdict
from src.biosyn.preprocesser import TextPreprocess
from ieaiaio import IOUtils as iou


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Parse input file (bel.<train/dev>.xml) to BioSyn format')
    parser.add_argument('--corpus', required=True, help = "Path to dir with corpus splits")
    parser.add_argument('--dicts', required=True, help = "Path to dir with parsed dictionaries")
    parser.add_argument('--outdir', required=True, help = "Path to dir with output dataset")
    
    return parser.parse_args()


class Symbol2IdMap(object):
    
    def __init__(self, dicts_dir, logger =None):
        
        self.logger = logger or logging.getLogger(self.__class__.__name__)
        self._mappings = self._load_symbol2id_by_dict(dicts_dir)
        
        self.discontinued = {"Mir190" : "Mir190a",
                             "Mir145" : "Mir145a"}
        
        
    def _load_symbol2id(self,file):
        
        mapping = {}
        with open(file) as infile:
            for line in infile:
                did,symbol = line.strip().split("||")
                mapping[symbol] = did
    
        return mapping


    def _load_symbol2id_by_dict(self, indir):
        
        mappings = {}
        
        for file in iou.iglob(indir, ext = "dict", recursive = True):
            name = iou.fname(file).replace(".dict","").upper()
            if name == "GOBP":
                continue
            mappings[name] = self._load_symbol2id(file)
        
        return mappings    
    
    
    def __call__(self,symbol, dictname):
        
        mappable = True
        
        if dictname not in self._mappings:
            raise ValueError("Dictionary {} not available!".format(dictname))
        
        mapping = self._mappings[dictname]
        
        if symbol not in mapping:
            if symbol in self.discontinued:
                self.logger.warning("Mapping discontinued symbol `{}` to `{}`!".format(symbol,self.discontinued.get(symbol)))
                symbol = self.discontinued.get(symbol)
            else:
                mappable = False
                self.logger.error("Symbol `{}` not in `{}` dictionary!".format(symbol,dictname))
            
        did = mapping.get(symbol) if mappable else None
        
        return did




class DatasetPreprocessor(object):
    
    def __init__(self, dicts, dicts_dir, logger = None):
        
        self.logger = logger or logging.getLogger(self.__class__.__name__)
        
        self.dicts = dicts
        
        self.symbol2id = Symbol2IdMap(dicts_dir)
        
        self.tpp = TextPreprocess(lowercase=True, remove_punctuation=True)
                        
        
    def get_split_from_path(self,filepath):
        
        split = None
        
        for splitname in ["train","dev","test"]:
            if splitname in filepath:
                split = splitname
                break
        
        if split is None:
            raise ValueError("Impossible to determine split from path: `{}`".format(filepath))
        
        return split
    
    def _get_anns_by_dict(self, passage):
        
        anns_by_dict = defaultdict(list)
                
        for ann in passage.annotations:
            for dictname in self.dicts:
                if ann.infons.get(dictname) is not None:
                    anns_by_dict[dictname].append(ann)
        
        return anns_by_dict
    
    
    def _write_concept_file(self, sentence_id, anns_by_dict, split, outdir):
        
        for dictname, anns in anns_by_dict.items():
            
            splitdir = "processed_{}".format(split)
            
            filepath = iou.join_paths([outdir, dictname, splitdir , ".".join([sentence_id,"concept"])])
            
            with open(filepath, "w") as outfile:
                for ann in anns:
                    
                    concept = self.biocann2concept(sentence_id = sentence_id, 
                                                   ann = ann,
                                                   dictname = dictname)
                    
                    if concept is not None:
                    
                        outfile.write("{}\n".format(concept))
                    
                    else:
                        
                        self.logger.warning("Skipping example (`{}`) in sentence {}! Impossible to determine CUI".format(ann.text, sentence_id, ann.text))
                    
                    
    def biocann2concept(self, sentence_id, ann, dictname):
        
        cui = ann.infons.get(dictname)
        
        if dictname == "MESHD":
            
            cui = self.tpp.run(cui)
        
        if dictname !=  "GOBP":
        
            cui = self.symbol2id(cui, dictname)
            
        
        concept = None
        
        if cui is not None:
            
            offset = "|".join([str(ann.total_span.offset),str(ann.total_span.end)])
            
            concept = [sentence_id, offset, ann.infons.get("type"), ann.text, cui]
            
            concept = "||".join([str(x) for x in concept])
        
        return concept
    
    def init_outdirs(self, outdir):
        
        for dictname in self.dicts:
            for splitname in ["train","dev","traindev"]:
                path = iou.join_paths([outdir,dictname,"processed_{}".format(splitname)])
                iou.mkdir(path)

    def create_datasets(self, indir, outdir):
        
        self.init_outdirs(outdir)
                
        for filepath in iou.iglob(indir, ext = "xml"):
        
            self.logger.info("Processing: `{}`".format(filepath))
            
            splitname = self.get_split_from_path(filepath)
            
            collection = bioc.load(filepath)
            
            tot = 0
            empty_docs = []
            
            for document in collection.documents:
                
                tot += 1
                
                sentence_id = document.id
                
                if len(document.passages) == 0:
                    empty_docs.append(sentence_id)
                    continue
                    
                assert len(document.passages) == 1, "Expecting only one sentence per document! Found {} in {}".format(len(document.passages), sentence_id)
                for p in document.passages:
#                    pmid = p.infons.get("PMID")
                    
                    anns_by_dict = self._get_anns_by_dict(passage = p)
                                        
                    self._write_concept_file(sentence_id = sentence_id, 
                                             anns_by_dict = anns_by_dict, 
                                             outdir = outdir,
                                             split = splitname)
                    
                    if splitname in ["train","dev"]:
                        
                        self._write_concept_file(sentence_id = sentence_id, 
                                                 anns_by_dict = anns_by_dict, 
                                                 outdir = outdir,
                                                 split = "traindev")
            
            self.logger.info("Completed processing {} split! There were {} empty documents out of {}.".format(splitname.upper(), len(empty_docs), tot))
                        
if __name__ == "__main__":
    
    args = parse_args()
    
    dpp = DatasetPreprocessor(dicts = ["HGNC","MGI","GOBP","MESHD"],
                              dicts_dir = args.dicts)
    
    
    dpp.create_datasets(indir = args.corpus, outdir = args.outdir)
                                    
                    

