#!/bin/bash


# rm emtpy locations
sed -i '/<location offset\=\"\" length\=\"\"\/>/d' $1/train.bioc.xml 
# insert fake offset
sed -i 's/<offset\/>/<offset>0<\/offset>/g' $1/train.bioc.xml 
# change attr names to be compatible with python bioc
sed -i 's/offset=/length=/g' $1/train.bioc.xml 
sed -i 's/start=/offset=/g' $1/train.bioc.xml 

sed -i '/<location offset\=\"\" length\=\"\"\/>/d' $1/dev.bioc.xml 
sed -i 's/<offset\/>/<offset>0<\/offset>/g' $1/dev.bioc.xml 
sed -i 's/offset=/length=/g' $1/dev.bioc.xml 
sed -i 's/start=/offset=/g' $1/dev.bioc.xml 

