#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')



import argparse
import numpy as np
import pandas as pd
from ieaiaio import IOUtils as iou


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Get basic descriptive stats from the datasets')
    parser.add_argument('--indir', required=True, help = "Path to dir with parsed datasets")

    
    return parser.parse_args()


class Stats(object):
    
    def __init__(self, logger = None):
        
        self.logger = logger or logging.getLogger(self.__class__.__name__)
        
    
    def _get_descriptive_stats(self, splitpath):
        
        tot = 0
        cuis = set()
        mentions = set()
        
        for filepath in iou.iglob(splitpath, ext = "concept"):
            
            with open(filepath) as infile:
                for line in infile:
                    tot += 1
                    elems = line.strip().split("||")
                    name = elems[-2]
                    cui = elems[-1]
                    cuis.add(cui)
                    mentions.add(name)
        
        
        m_lens = [len(m) for m in mentions]
        
        stats = {"tot" : tot,
                 "# unique cuis" : len(cuis),
                 "# unique mentions" : len(mentions),
                 'min mention len' : np.min(m_lens),
                 'max mention len' : np.max(m_lens),
                 'mean mention len' : round(np.mean(m_lens),2),
                 }
      
        return stats
        
        
    def run(self, indir):
        
        splits = ["train", "dev"]
                
        ds_stats = {}
        
        for dspath in iou.iglob(indir):
            
            ds_name = iou.fname(dspath)
            
            for split in splits:
                
                splitpath = iou.join_paths([dspath, "processed_{}".format(split)])
                
                if split not in ds_stats:
                    ds_stats[split] = {}
                    
                ds_stats[split][ds_name] = self._get_descriptive_stats(splitpath)
        
        
        
        for split in splits:
            print("\n\n")
            print("{}:".format(split.upper()))
            df = pd.DataFrame.from_dict(ds_stats[split], orient = "index")
            print(df)
            
                
                
if __name__ == "__main__":
    
    args = parse_args()
    
    stats = Stats()
        
    stats.run(args.indir)
