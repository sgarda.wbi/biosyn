#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

import os
import pickle
import numpy as np
from src.biosyn import (
    DictionaryDataset,
    BioSyn,
    TextPreprocess
)


class BioSynConfig(object):
    
    def __init__(self, model_dir, dict_path, use_cuda, topk = 5, max_len = 25, for_disease_entity = False, dict_cache = None):
        self.model_dir = model_dir
        self.max_len = max_len
        self.use_cuda = use_cuda
        self.dict_path = dict_path
        self.topk = topk
        self.dict_cache = dict_cache
        self.for_disease_entity = for_disease_entity
        

class BioSynEntityLinker(object):
    
    def __init__(self, config):
        self.config = config
        self.model = BioSyn()
        self.textpp = TextPreprocess(lowercase=True, remove_punctuation=True)

        self.model.load_model(path = self.config.model_dir, 
                              max_length = self.config.max_len, 
                              use_cuda = self.config.use_cuda)    
        
        dictionary, dict_sparse_embeds, dict_dense_embeds = self.cache_or_load_dictionary(self.config.dict_path)
        
        self.dictionary = dictionary
        self.dict_sparse_embeds = dict_sparse_embeds
        self.dict_dense_embeds = dict_dense_embeds
    
    def cache_or_load_dictionary(self, dict_path):
        """
        Create dictionary: dense and sparse representations
        
        Caching at in `os.getcwd()/cached_DICT.pkl`
        """
        
        dictionary_name = os.path.splitext(os.path.basename(dict_path))[0]
        cached_dictionary_path = os.path.join(os.path.dirname(dict_path), dictionary_name)
    
    
        # If exist, load the cached dictionary
        if os.path.exists(cached_dictionary_path):
            with open(cached_dictionary_path, 'rb') as fin:
                cached_dictionary = pickle.load(fin)
            print("Loaded dictionary from cached file {}".format(cached_dictionary_path))
    
            dictionary, dict_sparse_embeds, dict_dense_embeds = (
                cached_dictionary['dictionary'],
                cached_dictionary['dict_sparse_embeds'],
                cached_dictionary['dict_dense_embeds'],
            )
    
        else:
            dictionary = DictionaryDataset(dictionary_path = dict_path).data
            dictionary_names = dictionary[:,0]
            dict_sparse_embeds = self.model.embed_sparse(names=dictionary_names, show_progress=True)
            dict_dense_embeds = self.model.embed_dense(names=dictionary_names, show_progress=True)
            cached_dictionary = {
                'dictionary': dictionary,
                'dict_sparse_embeds' : dict_sparse_embeds,
                'dict_dense_embeds' : dict_dense_embeds
            }
    
            if not os.path.exists('./tmp'):
                os.mkdir('./tmp')
            with open(cached_dictionary_path, 'wb') as fin:
                pickle.dump(cached_dictionary, fin)
            print("Saving dictionary into cached file {}".format(cached_dictionary_path))
    
        return dictionary, dict_sparse_embeds, dict_dense_embeds
    
    
    
    def link(self, mention):
        """
        Retrieve topk IDs (and their names) for a given mention.
        
        Args:
            mention (str) : mention to normalize
        
        Return:
            names (np.array) : array of strings (dict names)
            ids (np.array) : array of strings (dict ids)
            
        """
        
        if self.config.for_disease_entity:
        
            mention = self.textpp.run(mention)
        
        mention_sparse_embeds = self.model.embed_sparse(names=[mention])
        mention_dense_embeds = self.model.embed_dense(names=[mention])
        
        sparse_score_matrix = self.model.get_score_matrix(query_embeds=mention_sparse_embeds,
                                                          dict_embeds=self.dict_sparse_embeds)
        
        
        dense_score_matrix = self.model.get_score_matrix(query_embeds=mention_dense_embeds,
                                                         dict_embeds=self.dict_dense_embeds)
        
        
        sparse_weight = self.model.get_sparse_weight().item()
        
        hybrid_score_matrix = sparse_weight * sparse_score_matrix + dense_score_matrix

        hybrid_candidate_idxs = self.model.retrieve_candidate(score_matrix = hybrid_score_matrix, topk = 5)

        # get predictions from dictionary
        predictions = self.dictionary[hybrid_candidate_idxs].squeeze(0)
        
        predicted_names = np.asarray([p[0] for p in predictions])
        predicted_ids = np.asarray([p[1] for p in predictions])        
        
        return predicted_names, predicted_ids
    
    
if __name__ == "__main__":
    
    import argparse
    
    
    def parse_args():    
        parser = argparse.ArgumentParser(description='Test BioSyn as pipeline component')
        parser.add_argument('--model-dir', required=True, help = "Path to trained BioSyn model")
        parser.add_argument('--dict-path', required=True, help = "Path to dev/test dictionary")
        parser.add_argument('--use-cuda', action = "store_true", help = "GPU available")
        parser.add_argument('--topk', type = int, default = 5, help = "Top predictions to return")
        parser.add_argument('--max-length', type = int, default = 25,  help = "Maximum length (input truncated)")
        parser.add_argument('--for-disease-entity', action = "store_true", help = "Preprocess text (only for MESHD)")
        
        return parser.parse_args()
        
    
    args = parse_args()
    
    config = BioSynConfig(model_dir = args.model_dir, 
                          dict_path = args.dict_path, 
                          use_cuda = args.use_cuda, 
                          topk = args.topk, 
                          max_len = args.max_length, 
                          for_disease_entity = args.for_disease_entity)
    
    
    linker = BioSynEntityLinker(config =config)
    
    
    names, ids = linker.link(mention = "Osteoporosis")
    
    print(names)
    print(ids)
    
    