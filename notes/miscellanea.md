# Miscellanea

## Default trian command

### Local

python train.py --model_dir ~/data/event_extraction/biosyn/biobert_v1/ --train_dictionary_path ~/data/event_extraction/biosyn/datasets/MESHD/train_dictionary.txt --train_dir ~/data/event_extraction/biosyn/datasets/MESHD/processed_traindev --output_dir ~/data/event_extraction/biosyn/datasets/MESHD/model --topk 10 --epoch 1 --train_batch_size 8 --initial_sparse_weight 0 --learning_rate 1e-5 --max_length 25 --dense_ratio 0.5

### Server

#### train
Just change the dictionary in the path

CUDA_VISIBLE_DEVICES=0 python train.py --model_dir ~/data/biosyn/biobert_v1/ --train_dictionary_path ~/data/biosyn/datasets/HGNC/train_dictionary.txt --train_dir ~/data/biosyn/datasets/HGNC/processed_traindev/ --output_dir ~/data/biosyn/datasets/HGNC/results --use_cuda --epoch 5 --train_batch_size 16 --learning_rate 1e-5 --max_len 25

#### eval
Just change the dictionary in the path

CUDA_VISIBLE_DEVICES=0 python eval.py --model_dir ~/data/biosyn/datasets/HGNC/results/ --dictionary_path ~/data/biosyn/datasets/HGNC/dev_dictionary.txt --data_dir ~/data/biosyn/datasets/HGNC/processed_dev/ --use_cuda --max_length 25 --output_dir ~/data/biosyn/datasets/HGNC/evaluation --save_predictions

